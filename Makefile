# Every Makefile should contain this line to avoid trouble on systems where the SHELL variable might be inherited from the environment.
SHELL = /bin/sh
# Makefile variables
# Directory where the program should be installed
DESTDIR ?= /usr/local/bin
# Program for compiling C++ programs; default g++. 
CXX ?= g++
# Extra flags to give to the C++ compiler. 
CXXFLAGS = -g  -Wall -Wextra -Wpedantic -Werror 
# Extra flags to give to compilers when they are supposed to invoke the linker, ‘ld’, such as -L. Libraries (-lfoo) should be added to the LDLIBS variable instead.
LDFLAGS ?=
# Library flags or names given to compilers when they are supposed to invoke the linker, ‘ld’. Non-library linker flags, such as -L, should go in the LDFLAGS variable.
LDLIBS =
# User variables
# Binary file name
target = $(shell basename $(CURDIR))
# Source files directory
source_dir := ./src
# Build files directory
build_dir ?= ./build
# Automatic variables
# Source files
source_files := $(wildcard $(source_dir)/*.cpp)
# Object files
object_files := $(patsubst $(source_dir)%.cpp,$(build_dir)%.o, $(source_files))
# Dependency files
dependency_files := $(object_files:.o=.d)

# link all .o to generate the program executable
$(target): $(object_files)
	@echo "linking $(object_files) to $(target)"
	$(CXX) $(LDFLAGS) $(object_files) -o $@

# Include all .d files
-include $(dependency_files)

# compile all .cpp to .o
# | $(build_dir) is only called if the build directory doesn't exists
$(build_dir)/%.o:$(source_dir)/%.cpp | $(build_dir)
	@echo "compiling $< to $@"
	$(CXX) $(CXXFLAGS) -MMD -c $< -o $@

# create the build directory
$(build_dir):
	@echo "creating dir"
	mkdir -p $(build_dir)

.PHONY: clean mostlyclean install install-linux uninstall uninstall-linux release release-linux release-windows

# Delete all files that are normally created by running make. 
clean:
	rm -rf $(build_dir) $(target) $(target).exe

# Like ‘clean’, but may refrain from deleting a few files that people normally don’t want to recompile.
# Will delete all object file without deleting the binaries files
mostlyclean:
	rm -rf $(build_dir)

# Build the linux executable
linux:
	+$(MAKE) build_dir=./build/unix

# Build the windows executable
windows:
	+$(MAKE) CXX=x86_64-w64-mingw32-c++ build_dir=./build/win

# Copy the executable file into a directory that users typically search for commands $(DESTDIR).
install: $(target)
	cp $(target) $(DESTDIR)/$(target)

install-linux:
	+$(MAKE) install build_dir=./build/unix

# Remove the executable from the installation directory $(DESTDIR).
uninstall:
	rm $(DESTDIR)/$(target)

uninstall-linux:
	+$(MAKE) uninstall

# Build a version of this project with static libraries.
release: $(target)
	+$(MAKE) LDFLAGS="-static"

release-linux:
	+$(MAKE) release build_dir=./build/unix

release-windows:
	+$(MAKE) release CXX=x86_64-w64-mingw32-c++ build_dir=./build/win

