/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILE_H
#define FILE_H

#include <filesystem>
#include <string>
#include <vector>
#include <fstream>

class file
{

    std::filesystem::path path;
    std::streampos position;

public:
    /**
     * @brief Check if the file with the given name exists in the current directory.
     *
     * @param name The name of the file to check.
     * @return true if the file exists; false if the file doesn't exist.
     */
    static bool file_exist(const std::string &name);

    /**
     * @brief Check if the file with the given name exists in the specified path.
     *
     * @param name The name of the file to check.
     * @param path The path where the file should exist.
     * @return true if the file exists; false if the file doesn't exist.
     */
    static bool file_exist(const std::string &name, const std::string &path);

    /**
     * @brief Constructor: Initialize a file object with a specified path.
     *
     * @param path The path to the file.
     */
    file(const std::filesystem::path &path);

    /**
     * @brief Constructor: Initialize a new file object with the given name and path.
     *
     * @param name The name of the file.
     * @param path The path to the file.
     * @param create (Optional) If true, create the file if it doesn't exist.
     */
    file(const std::string &name, const std::string &path, const bool create = false);

    /**
     * @brief Constructor: Initialize a new file object with the given name.
     *
     * @param name The name of the file.
     * @param create (Optional) If true, create the file if it doesn't exist.
     */
    file(const std::string &name, const bool create = false);

    /**
     * @brief Copy the current file to another location.
     *
     * @param to The location to where the file will be copied.
     * @return true if the file was successfully copied; false if the file wasn't copied.
     */
    bool copy(const std::filesystem::path &to) const;

    /**
     * @brief Check if the file exists.
     *
     * @return true if the file exists; false if the file doesn't exist.
     */
    bool exist() const;

    /**
     * @brief Check if the file path exists.
     *
     * @return true if the file path exists; false if the file path doesn't exist.
     */
    bool exist_path() const;

    /**
     * @brief Get the name of the file.
     *
     * @return The name of the file as a string.
     */
    std::string get_name() const;

    /**
     * @brief Get the path of the file.
     *
     * @return The path of the file as a string.
     */
    std::string get_path() const;

    /**
     * @brief Read the entire file and return its content as a vector of strings.
     *
     * @return A vector of strings representing the lines in the file.
     */
    std::vector<std::string> read() const;

    /**
     * @brief Read a single line from the file.
     *
     * @return A string representing the read line.
     */
    std::string read_line();

    /**
     * @brief Delete the current file.
     *
     * @return true if the file was deleted; false if the file wasn't deleted.
     */
    bool remove();

    /**
     * @brief Write a string to the file.
     *
     * @param line The string to write to the file.
     * @param overwrite (Optional) If true, overwrite the file content; otherwise, append to the file.
     */
    void write(const std::string &line, const bool overwrite = false);

    /**
     * @brief Write a vector of strings to the file.
     *
     * @param lines The vector of strings to write to the file.
     * @param overwrite (Optional) If true, overwrite the file content; otherwise, append to the file.
     */
    void write(const std::vector<std::string> &lines, const bool overwrite = false);
};

#endif