/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
// headers
#include "cipher.h"
#include "binary.h"
// c libraries
#include <cmath>
#include <cstring>
// c++ libraries
#include <string>
#include <iostream>
#include <random>
#include <algorithm>

const char cipher::charset[] = {
    ' ',
    '!',
    '"',
    '#',
    '$',
    '%',
    '&',
    '\'',
    '(',
    ')',
    '*',
    '+',
    ',',
    '-',
    '.',
    '/',
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    ':',
    ';',
    '<',
    '=',
    '>',
    '?',
    '@',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
    '[',
    '\\',
    ']',
    '^',
    '_',
    '`',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
    '{',
    '|',
    '}',
    '\0'};

// todo change range to 32-125
const std::set<noise> cipher::generate_noise_charset()
{

    std::random_device random;

    std::set<noise> encode_charset;
    size_t counter = 0;
    size_t charset_size = std::strlen(cipher::charset);
    while (counter < charset_size)
    {
        char temp_noise[4] = {
            char(random() % (cipher::MAXIMUM_CHARSET - cipher::MINIMUM_CHARSET) + cipher::MINIMUM_CHARSET),
            char(random() % (cipher::MAXIMUM_CHARSET - cipher::MINIMUM_CHARSET) + cipher::MINIMUM_CHARSET),
            char(random() % (cipher::MAXIMUM_CHARSET - cipher::MINIMUM_CHARSET) + cipher::MINIMUM_CHARSET),
            char(random() % (cipher::MAXIMUM_CHARSET - cipher::MINIMUM_CHARSET) + cipher::MINIMUM_CHARSET)};

        if (encode_charset.insert(noise(temp_noise, 4)).second)
            counter++;
    };

    return encode_charset;
}

const std::map<char, noise> cipher::generate_encode_noise_map(const std::set<noise> &charset)
{
    size_t index = 0;
    std::map<char, noise> noise_map;
    for (auto noise : charset)
    {
        noise_map.insert({cipher::charset[index], noise});
        index++;
    }
    return noise_map;
};

const std::map<noise, char> cipher::generate_decode_noise_map(const std::set<noise> &charset)
{
    size_t index = 0;
    std::map<noise, char> noise_map;
    for (auto noise : charset)
    {
        noise_map.insert({noise, cipher::charset[index]});
        index++;
    }

    return noise_map;
};

void cipher::add_noise(std::string &data, const std::map<char, noise> noises)
{
    for (size_t index = 0; index < data.size(); index += 4)
    {
        data.insert(index, noises.at(data[index]).value);
        data.erase(index + 4, 1);
    }
}

void cipher::remove_noise(std::string &code, const std::map<noise, char> noises)
{
    for (size_t index = 0; index < code.size(); index++)
    {
        code.insert(index, 1, noises.at(noise(code.substr(index, 4), 4)));
        code.erase(index + 1, 4);
    }
}

unsigned int cipher::generate_key(const unsigned short number_of_bits)
{
    std::random_device random;
    return random() % ((unsigned long)std::pow(2, number_of_bits) - cipher::MAXIMUM_CHARSET);
}

void cipher::encode_noise(std::string &data, std::string &key,  const std::string salt, const unsigned short number_of_bits)
{
 
    std::string code;
    auto salt_it = salt.begin();
    for (auto data_it = data.begin(); data_it != data.end(); data_it++, salt_it++)
    {
        // cycle through the salt 
        if(salt_it == salt.end())
            salt_it = salt.begin();

        const unsigned int random_key = cipher::generate_key(number_of_bits);

        key.append(std::to_string(random_key));
        code.append(std::to_string( (*data_it ^ *salt_it) + random_key));

        if ((data_it + 1) != data.begin())
        {
            code.push_back(' ');
            key.push_back(' ');
        }
    }

    data = code;
}

void cipher::decode_noise(std::string &code, const std::string &key, const std::string salt)
{
    std::string text;

    auto code_it = code.begin();
    auto key_it = key.begin();
    auto salt_it = salt.begin();

    while (code_it != code.end())
    {

        if(salt_it == salt.end())
            salt_it = salt.begin();

        auto code_split_it = std::find(code_it, code.end(), ' ');

        auto key_split_it = std::find(key_it, key.end(), ' ');

        text.push_back(
            (std::stoi(std::string(code_it, code_split_it))- std::stoi(std::string(key_it, key_split_it)))
            ^ *salt_it
        );

        code_it = code_split_it + 1;
        key_it = key_split_it + 1;
        salt_it++;
    }

    code = text;
}

void cipher::convert_to_binary(std::string &data, const unsigned short number_of_bits)
{
    size_t index = 0;
    while (index < data.size())
    {

        size_t cut_at = data.find_first_of(' ');
        if (cut_at == ::std::string::npos)
            cut_at = data.size();

        unsigned int temp = std::stoi(data.substr(index, cut_at - index));
        binary binary(temp, number_of_bits);
        data.erase(index, cut_at - index + 1);
        data.insert(index, binary.to_string());

        index += number_of_bits;
    }
}

void cipher::convert_from_binary(std::string &code, const unsigned short number_of_bits)
{
    size_t index = 0;
    while (index < code.size())
    {

        size_t cut_at = index + number_of_bits;
        binary temp = binary(code.substr(index, cut_at - index), false);
        code.erase(index, cut_at - index);
        code.insert(index, std::to_string(temp.to_uint()) + " ");

        index = code.find_first_of(' ', index) + 1;
    }
}

void cipher::encode(std::string &data, std::string &key, std::string &noise_charset,  const std::string salt, const int8_t level)
{
    if (level == -1)
        return;

    const std::set<noise> temp_noise_charset = cipher::generate_noise_charset();

    const std::map<char, noise> encode_noise_map = cipher::generate_encode_noise_map(temp_noise_charset);

    cipher::add_noise(data, encode_noise_map);

    cipher::encode_noise(data, key, salt, 8);

    cipher::convert_to_binary(data, 8);

    noise_charset = noise::to_string(temp_noise_charset);
}

void cipher::encode(std::vector<std::string> &data, std::vector<std::string> &key, std::string &charset, const std::string salt, const int8_t level)
{
    if (level == -1)
        return;

    const std::set<noise> temp_noise_charset = cipher::generate_noise_charset();

    const std::map<char, noise> encode_noise_map = cipher::generate_encode_noise_map(temp_noise_charset);

    key.resize(data.size());
    for (auto data_it = data.begin(), key_it = key.begin(); data_it != data.end(); data_it++, key_it++)
    {
        cipher::add_noise(*data_it, encode_noise_map);

        cipher::encode_noise(*data_it, *key_it, salt, 8);

        cipher::convert_to_binary(*data_it, 8);
    }
    charset = noise::to_string(temp_noise_charset);
}

void cipher::decode(std::string &code, const std::string &key, const std::string &charset, const std::string salt)
{

    cipher::convert_from_binary(code, 8);

    cipher::decode_noise(code, key, salt);

    const std::set<noise> temp_noise_charset = noise::to_set(charset, 4);
    const std::map<noise, char> decode_noise_map = cipher::generate_decode_noise_map(temp_noise_charset);

    cipher::remove_noise(code, decode_noise_map);
}

void cipher::decode(std::vector<std::string> &code, const std::vector<std::string> &key, const std::string &charset, const std::string salt)
{

    const std::set<noise> temp_noise_charset = noise::to_set(charset, 4);
    const std::map<noise, char> decode_noise_map = cipher::generate_decode_noise_map(temp_noise_charset);

    auto code_it = code.begin();
    auto key_it = key.begin();
    while (code_it != code.end())
    {
        cipher::convert_from_binary(*code_it, 8);

        cipher::decode_noise(*code_it, *key_it, salt);

        cipher::remove_noise(*code_it, decode_noise_map);

        code_it++;
        key_it++;
    }
}