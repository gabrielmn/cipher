/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "binary.h"
// c libraries
#include <cmath>
// c++ libraries
#include <limits>
#include <stdexcept>
#include <algorithm>

binary::binary(signed int number, const std::size_t number_of_bits)
{

    if (number >= 0)
    {
        if (number != 0 && (size_t)std::log2(std::abs(number)) + 1 >= number_of_bits)
        {
            throw std::invalid_argument("number too big for this amount of bits");
        }
    }
    else
    {
        if (((size_t)std::log2(std::abs(number + 1)) + 1) >= number_of_bits)
        {
            throw std::invalid_argument("number too big for this amount of bits");
        }
    }

    // special cases
    if (number == 0)
    {
        this->is_signed = true;
        this->bits = std::string(number_of_bits, '0');
        return;
    }

    if (number == std::numeric_limits<signed int>::min() && number_of_bits == 32)
    {
        this->is_signed = true;
        this->bits = std::string(number_of_bits, '0');
        this->set(0, true);
        return;
    }

    if (number > 0)
    {
        while (number > 0)
        {
            this->bits.append(std::to_string(number % 2));
            number /= 2;
        }

        // fill string to match number of bits
        if (this->bits.size() < number_of_bits)
            this->bits.append(std::string(number_of_bits - this->bits.size(), '0'));

        this->reverse();

        this->is_signed = true;
    }
    else
    {
        number = std::abs(number);
        while (number > 0)
        {
            this->bits.append(std::to_string(number % 2));
            number /= 2;
        }

        // fill string to match number of bits
        if (this->bits.size() < number_of_bits)
            this->bits.append(std::string(number_of_bits - this->bits.size(), '0'));

        this->reverse();
        this->flip();

        (*this) += binary(1, number_of_bits); // sum current binary with binary of 1
        this->is_signed = true;
    }
}

binary::binary(unsigned int number, std::size_t number_of_bits)
{

    if ((size_t)std::log2(number) + 1 > number_of_bits)
    {
        throw std::invalid_argument("number too big for this amount of bits");
    }

    // special cases
    if (number == 0)
    {
        this->is_signed = false;
        this->bits = std::string(number_of_bits, '0');
        return;
    }

    while (number > 0)
    {
        this->bits.append(std::to_string(number % 2));
        number /= 2;
    }

    // fill string to match number of bits
    if (this->bits.size() < number_of_bits)
        this->bits.append(std::string(number_of_bits - this->bits.size(), '0'));

    this->reverse();

    this->is_signed = false;
}

binary::binary(signed long number, std::size_t number_of_bits)
{
    throw "not implemented";
}

binary::binary(unsigned long number, std::size_t number_of_bits)
{
    throw "not implemented";
}

binary::binary(const std::string bits, const bool is_signed)
{
    this->bits = bits;
    this->is_signed = is_signed;
}

size_t binary::count() const
{
    size_t counter = 0;
    for (auto it = this->bits.begin(); it != this->bits.end(); it++)
    {
        if (*it == '1')
            counter++;
    }

    return counter;
}

void binary::flip()
{

    for (auto it = this->bits.begin(); it != this->bits.end(); it++)
    {
        if (*it == '0')
            *it = '1';
        else
            *it = '0';
    }
}

void binary::flip(size_t index)
{

    if (index > this->bits.size())
        throw std::out_of_range("index out of range");

    if (this->bits[index] == '0')
        this->bits[index] = '1';
    else
        this->bits[index] = '0';
}

void binary::reset()
{
    this->bits = std::string(this->bits.size(), '0');
}

void binary::reset(size_t index)
{
    if (index > this->bits.size())
        throw std::out_of_range("index out of range");

    this->bits[index] = '0';
}

void binary::reverse()
{
    std::reverse(this->bits.begin(), this->bits.end());
}

void binary::set()
{
    std::replace(this->bits.begin() + 1, this->bits.end(), '0', '1');
}

void binary::set(const size_t index, const bool value)
{

    if (index > this->bits.size())
        throw std::out_of_range("index out of range");

    if (value)
        this->bits[index] = '1';
    else
        this->bits[index] = '0';
}

signed int binary::to_int() const
{
    if (this->bits.size() > 32)
        throw std::out_of_range("number to big to fit in 32 bits");

    // positive number
    if (this->bits[0] == '0')
    {
        signed int number = 0;
        for (int index = this->bits.size(); index > 0; index--)
        {
            if (this->bits[index] == '1')
                number = +(int)std::pow(2, index);
        }

        return number;
    }
    // negative number
    else
    {
        // TODO negative number
        throw "not implemented";
    }
}

signed long binary::to_long() const
{   
    // TODO 
    throw "not implemented";
    // return 0;
}

std::string binary::to_string() const
{
    return this->bits;
}

unsigned int binary::to_uint() const
{
    // TODO throw error if current binary is signed due to possible errors if the binary represents a negative number
    if (this->bits.size() > 32)
        throw std::out_of_range("number to big to fit in 32bits");

    signed int number = 0;
    size_t size = this->bits.size() - 1;
    for (size_t index = 0; index <= size; index++)
    {
        if (this->bits[index] == '1')
            number += (int)std::pow(2, size - index);
    }

    return number;
}

unsigned long binary::to_ulong() const
{
    // TODO 
    throw "not implemented";
}

binary &binary::operator+=(const binary &rhs)
{

    if (rhs.bits.size() > this->bits.size())
        throw std::invalid_argument("parameter too big");

    bool carry_bit = false;
    for (int index = this->bits.size() - 1; index >= 0; index--)
    {
        if (carry_bit)
        {
            if (this->bits[index] == '0' && rhs.bits[index] == '0')
            {
                carry_bit = false;
                this->bits[index] = '1';
            }
            else if (this->bits[index] == '1' && rhs.bits[index] == '0')
            {
                this->bits[index] = '0';
            }
        }
        else
        {
            if (this->bits[index] == '0' && rhs.bits[index] == '1')
            {
                this->bits[index] = '1';
            }
            else if (this->bits[index] == '1' && rhs.bits[index] == '1')
            {
                this->bits[index] = '0';
                carry_bit = true;
            }
        }
    }

    if (carry_bit)
        throw std::overflow_error("overflow bit");

    return *this;
}

binary operator+(binary lhs, const binary &rhs)
{
    lhs += rhs;
    return lhs;
}
