/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// parameters
// -e --encrypt
// -d --decrypt
//    --file
//    --key
//    --charset
//    --salt
//    --help
//    --version

#include <iostream>
#include <cstring>
#include <cstdlib>
// #include <string>
#include "file.h"
#include "cipher.h"
#include <random>

char *_strndup(const char *str, size_t size);

void print_help();

void print_version();

int main(int argc, char *argv[])
{

    bool enable_encrypt = false;
    bool enable_decrypt = false;

    file *source_file = nullptr, *key_file = nullptr, *charset_file = nullptr;
    std::string salt;

    for (int i = 1; i < argc; i++)
    {
        // full name
        if (argv[i][0] == '-' && argv[i][1] == '-')
        {
            if (std::strcmp(argv[i], "--encrypt") == 0)
            {
                enable_encrypt = true;
            }
            else if (strcmp(argv[i], "--decrypt") == 0)
            {
                enable_encrypt = true;
            }
            else if (strncmp(argv[i], "--file=", 7) == 0)
            {
                const size_t file_length = (std::strlen(argv[i]) - 7);
                source_file = new file(std::filesystem::path(_strndup(argv[i] + 7, file_length)));
            }
            else if (strncmp(argv[i], "--key=", 6) == 0)
            {
                const size_t key_length = (std::strlen(argv[i]) - 6);
                key_file = new file(std::filesystem::path(_strndup(argv[i] + 6, key_length)));
            }
            else if (strncmp(argv[i], "--charset=", 10) == 0)
            {
                const size_t charset_length = (std::strlen(argv[i]) - 10);
                charset_file = new file(std::filesystem::path(_strndup(argv[i] + 10, charset_length)));
            }
            else if (strncmp(argv[i], "--salt=", 7) == 0)
            {
                const size_t salt_length = (std::strlen(argv[i]) - 7);
                salt = _strndup(argv[i] + 7, salt_length);
            }
            else if (strcmp(argv[i], "--help") == 0)
            {
                print_help();
                return 0;
            }
            else if (strcmp(argv[i], "--version") == 0)
            {
                print_version();
                return 0;
            }
        }
        // short name
        else if (argv[i][0] == '-')
        {
            if (std::strcmp(argv[i], "-e") == 0)
            {
                enable_encrypt = true;
            }
            else if (strcmp(argv[i], "-d") == 0)
            {
                enable_decrypt = true;
            }
        }
    }

    // missing mandatory options
    if (!enable_encrypt && !enable_decrypt)
        return EXIT_FAILURE;

    // to many mandatory options
    if (enable_encrypt && enable_decrypt)
        return EXIT_FAILURE;

    // missing source file to encrypt
    if (enable_encrypt && source_file == nullptr)
        return EXIT_FAILURE;

    if (enable_encrypt && source_file != nullptr)
    {
        std::vector<std::string> key, data = source_file->read();
        std::string charset;
        cipher::encode(data, key, charset, salt, 2);
        source_file->write(data, true);

        if (key_file == nullptr)
            key_file = new file(source_file->get_name() + ".key", source_file->get_path(), true);

        if (charset_file == nullptr)
            charset_file = new file(source_file->get_name() + ".charset", source_file->get_path(), true);

        key_file->write(key);
        charset_file->write(charset);

        return EXIT_SUCCESS;
    }

    // missing source file to decrypt
    if (enable_decrypt && source_file == nullptr)
        return EXIT_FAILURE;

    if (enable_decrypt && source_file != nullptr)
    {
        if(key_file == nullptr)
            key_file = new file(source_file->get_name() + ".key", source_file->get_path());
        
        if(charset_file == nullptr)
            charset_file = new file(source_file->get_name() + ".charset", source_file->get_path());
        
        std::vector<std::string> code = source_file->read(), key = key_file->read();
        std::string charset = charset_file->read_line();
        cipher::decode(code, key, charset, salt);

        source_file->write(code, true);

        key_file->remove();
        charset_file->remove();

        return EXIT_SUCCESS;
    }
}

char *_strndup(const char *str, size_t size)
{

    if (str == nullptr)
        throw std::invalid_argument("null pointer");

    char *buffer = new char[size];

    memcpy(buffer, str, size);

    return buffer;
}

void print_help()
{
    std::string help("");
    help.append("Usage: cipher [OPTIONS]\n");
    help.append("Encrypt or decrypt a file using a custom cipher.\n");
    help.append("\n");
    help.append("Mandatory arguments to long options are mandatory for short options too.\n");
    help.append("  -d --decrypt\t\tdecrypt the input file.\n");
    help.append("  -e --encrypt\t\tencrypt the input file.\n");
    help.append("     --charset=CHARSET\t\tcharset generated while encrypting the file.\n");
    help.append("     --file=FILE\tfile to be encrypted or decrypted.\n");
    help.append("     --key=KEY\tkey generated while encrypting the file.\n");
    help.append("     --salt=SALT\tsalt used to encrypt or decrypt the file.\n");
    help.append("\n");
    help.append("     --help\t\t\tdisplay this help and exit\n");
    help.append("     --version\t\t\toutput version information and exit\n");
    help.append("\n");
    help.append("Operation-specific requirements:\n");
    help.append("  When using -d, --charset, --file, and --key are mandatory.\n");
    help.append("  When using -e, only --file should be used. The key and character set will be auto-generated with extensions .key and .charset, respectively.\n");
    help.append("\n");
    help.append("Examples:\n");
    help.append("  cipher -d --file=myfile.txt --key=mykey.key --charset=mycharset.charset   Decrypt text from a file using a specific key and character set.\n");
    help.append("  cipher -e --file=myfile.txt   Encrypt text from a file with an auto-generated key and character set.\n");

    std::cout << help << std::endl;
}

void print_version()
{
    std::string version("");
    version.append("cipher 1.0.0\n");
    version.append("Copyright (C) 2022 Gabriel M. Nori\n");
    version.append("License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\n");
    version.append("This is free software: you are free to change and redistribute it.\n");
    version.append("There is NO WARRANTY, to the extent permitted by law.\n\n");
    version.append("Written by Gabriel M. Nori.");

    std::cout << version << std::endl;
}