/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CIPHER_H
#define CIPHER_H

#include "noise.h"

#include <map>
#include <string>
#include <set>
#include <ostream>
#include <vector>


class cipher
{
    static const unsigned int MINIMUM_CHARSET = 65;
    static const unsigned int MAXIMUM_CHARSET = 91;

    static const char charset[];

    /**
     * @brief 
     * 
     * @return const std::set<noise> 
     */
    static const std::set<noise> generate_noise_charset();

    /**
     * @brief 
     * 
     * @param charset in
     * @return const std::map<char, noise> 
     */
    static const std::map<char, noise> generate_encode_noise_map(const std::set<noise> &charset);

    /**
     * @brief 
     * 
     * @param charset in
     * @return const std::map<noise, char> 
     */
    static const std::map<noise, char> generate_decode_noise_map(const std::set<noise> &charset);

    /**
     * @brief 
     * 
     * @param data inout
     * @param noise in
     */
    static void add_noise(std::string &data, const std::map<char, noise> noise);

    /**
     * @brief 
     * 
     * @param code inout 
     * @param noise in
     */
    static void remove_noise(std::string &code, const std::map<noise, char> noise);

    /**
     * @brief 
     * 
     * @param number_of_bits in
     * @return unsigned int 
     */
    static unsigned int generate_key(const unsigned short number_of_bits);

    /**
     * @brief 
     * 
     * @param data inout
     * @param key out
     * @param number_of_bits in 
     */
    static void encode_noise(std::string &data, std::string &key,  const std::string salt, const unsigned short number_of_bits);

    /**
     * @brief 
     * 
     * @param code inout 
     * @param key in
     */
    static void decode_noise(std::string &code, const std::string &key, const std::string salt);
    
    static void convert_to_binary(std::string &data, const unsigned short number_of_bits);

    static void convert_from_binary(std::string &code, const unsigned short number_of_bits);

public:

    /**
     * @brief
     *
     * @param data in_out
     * @param key out
     * @param charset out
     * @param level
     */
    static void encode(std::string &data, std::string &key, std::string &noise_charset, const std::string salt, const int8_t level = 0);

    static void encode(std::vector<std::string> &data, std::vector<std::string> &key, std::string &charset, const std::string salt, const int8_t level = 0);

    /**
     * @brief
     *
     * @param code inout
     * @param key in
     * @param charset in
     */
    static void decode(std::string &code, const std::string &key, const std::string &charset, const std::string salt);

    static void decode(std::vector<std::string> &code, const std::vector<std::string> &key, const std::string &charset, const std::string salt);
};

#endif