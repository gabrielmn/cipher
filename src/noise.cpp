/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "noise.h"
#include <stdexcept>

std::string noise::to_string(const std::set<noise> &noise_charset)
{
    std::string temp("");

    for(auto it = noise_charset.begin(); it != noise_charset.end(); it++)
        temp+= (*it).value;

    return temp;
}

std::set<noise> noise::to_set(const std::string &noise_charset, const size_t &size)
{
    std::set<noise> temp;
    for(auto it = noise_charset.begin(); it != noise_charset.end(); it+= size)
        temp.insert(noise(std::string(it, it + size), size));

    return temp;
}

noise::noise(const std::string &value, const size_t &size): size(size), value(value)
{
    if (this->value.size() > this->size)
        throw std::invalid_argument("value is bigger than size");
}

bool operator<(const noise &lhs, const noise &rhs)
{
    if (lhs.size != rhs.size)
        throw std::invalid_argument("arguments with different size");

    return lhs.value < rhs.value;
}

bool operator>(const noise &lhs, const noise &rhs)
{
    return rhs < lhs;
}

bool operator<=(const noise &lhs, const noise &rhs)
{
    return !(lhs > rhs);
}

bool operator>=(const noise &lhs, const noise &rhs)
{
    return !(lhs < rhs);
}

bool operator==(const noise &lhs, const noise &rhs)
{
    if (lhs.size != rhs.size)
        throw std::invalid_argument("arguments with different size");

    return lhs.value == rhs.value;
}

bool operator!=(const noise &lhs, const noise &rhs)
{
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, noise const &node)
{
    return os << node.value;
}
