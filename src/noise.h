/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NOISE_H
#define NOISE_H

#include <string>
#include <set>
#include <ostream>

struct noise
{

    static std::string to_string(const std::set<noise> &noise_charset);
    static std::set<noise> to_set(const std::string &noise_charset, const size_t &size);

    const size_t size;
    const std::string value;

    noise(const std::string &value, const size_t &size);

    friend bool operator<(const noise &lhs, const noise &rhs);
    friend bool operator>(const noise &lhs, const noise &rhs);
    friend bool operator<=(const noise &lhs, const noise &rhs);
    friend bool operator>=(const noise &lhs, const noise &rhs);
    friend bool operator>=(const noise &lhs, const noise &rhs);
    friend bool operator==(const noise &lhs, const noise &rhs);
    friend bool operator!=(const noise &lhs, const noise &rhs);

    friend std::ostream &operator<<(std::ostream &os, noise const &node);
};


#endif