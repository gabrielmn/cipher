/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "file.h"


bool file::file_exist(const std::string &name)
{
    return file::file_exist(name, std::filesystem::current_path().string());
}

bool file::file_exist(const std::string &name, const std::string &path)
{
    std::filesystem::path temp_path(path);
    temp_path += std::filesystem::path::preferred_separator;
    temp_path += name;
    return std::filesystem::exists(temp_path);
}

file::file(const std::filesystem::path &path)
{
    if (!path.has_filename())
        throw std::invalid_argument("not a path to a file");
    if (path.has_parent_path())
        this->path = path;
    else
    {
        this->path = std::filesystem::current_path();
        this->path += std::filesystem::path::preferred_separator;
        this->path += path.filename();
    }
}

file::file(const std::string &name, const std::string &path, const bool create)
{

    this->path = std::filesystem::path(path);
    this->path += std::filesystem::path::preferred_separator;
    this->path += name;

    if (create && !std::filesystem::exists(this->path))
    {
        if (!std::filesystem::exists(this->path.parent_path().c_str()))
        {
            std::filesystem::create_directories(this->path.parent_path().c_str());
        }

        std::fstream file(this->path.c_str(), std::ios::out);
        if (!file.is_open())
        {
            throw std::string("can't create file");
        }
        file.close();
    }
}

file::file(const std::string &name, const bool create)
{
#ifdef __unix__
    *this = file(name, std::filesystem::current_path(), create);
#elif __WIN32
    this->path = std::filesystem::current_path();
    this->path += std::filesystem::path::preferred_separator;
    this->path += name;

    if (create && !std::filesystem::exists(this->path))
    {
        if (!std::filesystem::exists(this->path.parent_path().c_str()))
        {
            std::filesystem::create_directories(this->path.parent_path().c_str());
        }

        std::fstream file(this->path.c_str(), std::ios::out);
        if (!file.is_open())
        {
            throw std::string("can't create file");
        }
        file.close();
    }
#endif
}

bool file::copy(const std::filesystem::path &to) const
{
    if (this->exist())
        return std::filesystem::copy_file(this->path, to);
    return false;
}

bool file::exist() const
{
    return std::filesystem::exists(this->path);
}

bool file::exist_path() const
{
    return std::filesystem::exists(this->path.parent_path().c_str());
}

std::string file::get_name() const
{
    return this->path.filename().string();
}

std::string file::get_path() const
{
    return this->path.parent_path().string();
}

std::vector<std::string> file::read() const
{
    std::fstream file(this->path.string(), std::ios::in);
    if (!file.is_open())
        throw std::string("can't open file");

    std::vector<std::string> temp;

    while(!file.eof()){
        std::string line;
        std::getline(file, line);
        temp.push_back(line);
    }

    file.close();
    return temp;
}

std::string file::read_line()
{
    std::fstream file(this->path.string(), std::ios::in);
    
    if (!file.is_open())
        throw std::string("can't open file");

    if(this->position == -1)
        this->position = 0;
    file.seekg(this->position);

    std::string line;
    std::getline(file, line);
    
    this->position = file.tellg();
    file.close();

    return line;
}

bool file::remove()
{
    if (this->exist())
        return std::filesystem::remove(this->path);
    return false;
}

void file::write(const std::string &line, const bool overwrite)
{
    std::fstream file(this->path.c_str(), std::ios::out | ((overwrite) ? std::ios::trunc : std::ios::app));
    if (!file.is_open())
        throw std::string("can't open file");
    file << line;
    file.close();
}

void file::write(const std::vector<std::string> &lines, const bool overwrite)
{
    std::fstream file(this->path.c_str(), std::ios::out | ((overwrite) ? std::ios::trunc : std::ios::app));
    if (!file.is_open())
        throw std::string("can't open file");
    
    for(auto it = lines.begin(); it != (lines.end() - 1) ; it++)
        file << *it << std::endl;
    
    file << *lines.rbegin();

    file.close();
}
