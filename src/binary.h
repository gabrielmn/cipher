/*
    This file is part of Cipher.
    Copyright (C) 2022  Gabriel M. Nori

    Cipher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BINARY_H
#define BINARY_H

#include <string>

/*
#########################################################################################
#----------------------------------------README-----------------------------------------#
#########################################################################################
This class uses two's complement instead of sign magnitude, given that it's
what is implemented to represent number on c++.
Because  of that the upper limit and lower limits of signed number will be different and will differ by 1 and the lower limit been the bigger one, thus std::abs(upper limit) != std::abs(lower limit) and std::abs(upper limit) == std::abs(lower limit + 1), for this reason all constructors that receive signed number will need to check the upper and lower limit before converting to binary.
*/

// TODO study change bits from string to boolean array or vector

/**
 * @brief Class to work with binary numbers.
 */
class binary
{

    bool is_signed;
    std::string bits;

public:
    /**
     * @brief Constructor for a signed integer.
     * @param number The signed integer to convert to binary.
     * @param number_of_bits The number of bits to use for the binary representation.
     */
    binary(signed int number, const std::size_t number_of_bits);

    /**
     * @brief Constructor for an unsigned integer.
     * @param number The unsigned integer to convert to binary.
     * @param number_of_bits The number of bits to use for the binary representation.
     */
    binary(unsigned int number, std::size_t number_of_bits);

    /**
     * @brief Constructor for a signed long integer.
     * @param number The signed long integer to convert to binary.
     * @param number_of_bits The number of bits to use for the binary representation.
     */
    binary(signed long number, std::size_t number_of_bits);

    /**
     * @brief Constructor for an unsigned long integer.
     * @param number The unsigned long integer to convert to binary.
     * @param number_of_bits The number of bits to use for the binary representation.
     */
    binary(unsigned long number, std::size_t number_of_bits);

    /**
     * @brief Constructor for a binary number from a string.
     * @param bits The binary string representation.
     * @param is_signed Whether the binary number is signed or not.
     */
    binary(const std::string bits, const bool is_signed);

    /**
     * @brief Returns the number of bits that are set to true.
     * @return size_t
     */
    size_t count() const;

    /**
     * @brief Flip all bits.
     */
    void flip();

    /**
     * @brief Flip a bit at the given index.
     * @param index The position of the bit to flip (from most significant to least significant).
     */
    void flip(size_t index);

    /**
     * @brief Set all bits to false.
     */
    void reset();

    /**
     * @brief Set a bit at the given index to false.
     * @param index The position of the bit to set (from most significant to least significant).
     */
    void reset(const size_t index);

    /**
     * @brief Reverse the order of the bits.
     */
    void reverse();

    /**
     * @brief Set all bits to true.
     */
    void set();

    /**
     * @brief Set a bit at the given index to a specific value.
     * @param index The position of the bit to set (from most significant to least significant).
     * @param value The value to set the bit to.
     */
    void set(const size_t index, const bool value);

    /**
     * @brief Return a signed int representation of the current binary number.
     * @return signed int - signed int representation of the current binary number.
     */
    signed int to_int() const;

    /**
     * @brief Return a signed long representation of the current binary number.
     * @return signed long - signed long representation of the current binary number.
     */
    signed long to_long() const;

    /**
     * @brief Return a string representation of the current binary number.
     * Uses '0' to represent bits with a value of false and '1' to represent bits with a value of true.
     * @return std::string - string representation of the current binary number.
     */
    std::string to_string() const;

    /**
     * @brief Return an unsigned int representation of the current binary number.
     * @return unsigned int - unsigned int representation of the current binary number.
     */
    unsigned int to_uint() const;

    /**
     * @brief Return an unsigned long representation of the current binary number.
     * @return unsigned long - unsigned long representation of the current binary number.
     */
    unsigned long to_ulong() const;

    // operators
    binary &operator+=(const binary &rhs);
    friend binary operator+(binary lhs, const binary &rhs);
};

#endif